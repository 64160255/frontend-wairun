import type Employee from "./Employee";

export default interface CheckInOut {
  id?: number;
  date: Date;
  time_in: Date;
  time_out: Date;
  total_hour: Number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  EmployeeId: number;
  employee: Employee;
}
