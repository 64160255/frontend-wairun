export default interface Store {
  id?: number;
  name: string;
  address_state: string;
  address_city: string;
  tel: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
