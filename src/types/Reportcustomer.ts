export default interface ReportCustomer {
  id?: number;
  name: string;
  tel: string;
  point: number;
}
