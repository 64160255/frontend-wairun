export default interface User {
  id?: number;
  name: string;
  password: string;
  address: string;
  tel: string;
  email: string;
  position: string;
  hourly_wage: number;

  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
