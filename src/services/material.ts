import type Material from "@/types/Material";
import http from "./axios";
function getMaterials(params: any) {
  return http.get("/material", { params: params });
}

function saveMaterials(material: {
  name: string;
  min_quantity: number;
  quantity: number;
  unit: string;
  price_per_unit: number;
}) {
  return http.post("/material", material);
}

function updateMaterials(id: number, material: Material) {
  return http.patch(`/material/${id}`, material);
}

function deleteMaterials(id: number) {
  return http.delete(`/material/${id}`);
}

export default {
  getMaterials,
  saveMaterials,
  updateMaterials,
  deleteMaterials,
};
