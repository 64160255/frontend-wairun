import http from "./axios";
function getCheckInOut() {
  return http.get("/checkinout");
}
function saveCheckInOut(username: string, password: string) {
  return http.post("/checkinout", { username, password });
}

function updateCheckInOut(id: number, username: string, password: string) {
  return http.patch(`/checkinout/${id}`, { username, password });
}

export default {
  getCheckInOut,
  saveCheckInOut,
  updateCheckInOut,
};
