import type User from "@/types/User";
import http from "./axios";
function getEmployees(params: any) {
  return http.get("/users", { params: params });
}
function getEmployeeById(id: number) {
  return http.get(`/users/${id}`);
}

function getEmployeesByTel(tel: string) {
  return http.get(`/users/tel/${tel}`);
}

function saveEmployee(employees: User) {
  return http.post("/users", employees);
}
function updateEmployee(id: number, employees: User) {
  return http.patch(`/users/${id}`, employees);
}
function daleteEmployee(id: number) {
  return http.delete(`/users/${id}`);
}
export default {
  getEmployeeById,
  getEmployees,
  saveEmployee,
  updateEmployee,
  daleteEmployee,
  getEmployeesByTel,
};

