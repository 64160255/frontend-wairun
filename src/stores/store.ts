import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Store from "@/types/Store";
import storeService from "@/services/store";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import Swal from "sweetalert2";

export const useStoreStore = defineStore("Store", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const stores = ref<Store[]>([]);
  const editedStore = ref<Store>({
    name: "",
    address_state: "",
    address_city: "",
    tel: "",
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedStore.value = {
        name: "",
        address_state: "",
        address_city: "",
        tel: "",
      };
    }
  });
  async function getStores() {
    loadingStore.isLoading = true;
    try {
      const res = await storeService.getStores();
      stores.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Store ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveStore() {
    loadingStore.isLoading = true;
    try {
      if (editedStore.value.id) {
        const res = await storeService.updateStore(
          editedStore.value.id,
          editedStore.value
        );
      } else {
        const res = await storeService.saveStore(editedStore.value);
      }

      dialog.value = false;
      await getStores();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Store ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteStore(id: number) {
    const confirmResult = await Swal.fire({
      title: "Are you sure?",
      text: "You will not be able to recover this store!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes",
      cancelButtonText: "Cancel",
    });

    if (confirmResult.isConfirmed) {
      try {
        const res = await storeService.deleteStore(id);
        await getStores();
        messageStore.showConfirm("ลบ Store เรียบร้อยแล้ว");
      } catch (e) {
        console.log(e);
        messageStore.showError("ไม่สามารถลบ Store ได้");
      }
    }

    loadingStore.isLoading = false;
  }
  function editStore(store: Store) {
    editedStore.value = JSON.parse(JSON.stringify(store));
    dialog.value = true;
  }
  return {
    stores,
    getStores,
    dialog,
    editedStore,
    saveStore,
    editStore,
    deleteStore,
  };
});
