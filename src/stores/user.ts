import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";
import userService from "@/services/user";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import Swal from "sweetalert2";
export const useUserStore = defineStore("User", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const users = ref<User[]>([]);
  const editedUser = ref<User>({
    name: "",
    password: "",
    address: "",
    tel: "",
    email: "",
    position: "",
    hourly_wage: 0,
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedUser.value = {
        name: "",
        password: "",
        address: "",
        tel: "",
        email: "",
        position: "",
        hourly_wage: 0,
      };
    }
  });
  async function getUsers() {
    loadingStore.isLoading = true;
    try {
      const res = await userService.getUsers();
      users.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล User ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveUser() {
    loadingStore.isLoading = true;
    try {
      if (editedUser.value.id) {
        const res = await userService.updateUser(
          editedUser.value.id,
          editedUser.value
        );
      } else {
        const res = await userService.saveUser(editedUser.value);
      }

      dialog.value = false;
      await getUsers();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก User ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteUser(id: number) {
    const confirmed = await Swal.fire({
      title: "Are you sure?",
      text: "You will not be able to recover this user!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes",
    });

    if (confirmed.isConfirmed) {
      loadingStore.isLoading = true;
      try {
        const res = await userService.deleteUser(id);
        await getUsers();
        messageStore.showConfirm("User deleted successfully");
      } catch (e) {
        console.log(e);
        messageStore.showError("Failed to delete user");
      }
      loadingStore.isLoading = false;
    }
  }
  function editUser(user: User) {
    editedUser.value = JSON.parse(JSON.stringify(user));
    dialog.value = true;
  }
  return {
    users,
    getUsers,
    dialog,
    editedUser,
    saveUser,
    editUser,
    deleteUser,
  };
});
