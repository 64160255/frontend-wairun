import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Customer from "@/types/Customer";
import customerService from "@/services/customer";
import reportService from "@/services/reports";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import Swal from "sweetalert2";
import type ReportCustomer from "@/types/Reportcustomer";

export const useCustomerStore = defineStore("Customer", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const keyword = ref("");
  const page = ref(1);
  // const take = ref(5);
  const lastPage = ref(3);
  const dialog = ref(false);
  const dialog2 = ref(false);
  const status = ref(true);
  const customerBypoint = ref<ReportCustomer[]>([]);
  const customerReport = ref<ReportCustomer[]>([]);
  const customers = ref<Customer[]>([]);
  const usingCus = ref<Customer>({
    name: "",
    tel: "",
    point: 0,
    start_date: "",
  });
  const editedCustomer = ref<Customer>({
    name: "",
    tel: "",
    point: 0,
    start_date: "",
  });
  watch(keyword, async (newKeyword, oldKeyword) => {
    await getCustomers();
  });
  watch(page, async (newPage, oldPage) => {
    await getCustomers();
  });
  watch(lastPage, async (newLastPage, oldLastPage) => {
    if (newLastPage < page.value) {
      page.value = 1;
    }
  });
  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedCustomer.value = { name: "", tel: "", point: 0, start_date: "" };
    }
  });
  async function getReportCustomer() {
<<<<<<< HEAD
<<<<<<< HEAD
    try {
      const res = await reportService.getCustomer();
      customerBypoint.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่มีข้อมูล Customer");
    }
=======
    
      const res = await reportService.getReportCustomer();
      customerReport.value = res.data[0];
    
>>>>>>> 5dc1ca8ec71ab3aca97b8774c81912ed6b46f721
=======
    const res = await reportService.getReportCustomer();
    customerReport.value = res.data[0];
>>>>>>> e7aee2d43791819729abceb821b08638da97b6d9
  }
  async function getCustomerByPoint() {
    try {
      const res = await reportService.getCustomerByPoint();
      customerBypoint.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่มีข้อมูล Customer");
    }
  }
  async function getCustomers() {
    loadingStore.isLoading = true;
    try {
      const res = await customerService.getCustomers({
        page: page.value,
        // take: take.value,
        keyword: keyword.value,
      });
      customers.value = res.data.data;
      lastPage.value - res.data.lastPage;
      console.log(customers.value);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Customer ได้");
    }
    loadingStore.isLoading = false;
  }
  async function saveCustomer() {
    loadingStore.isLoading = true;
    try {
      if (editedCustomer.value.id) {
        const res = await customerService.updateCustomer(
          editedCustomer.value.id,
          editedCustomer.value
        );
      } else {
        const res = await customerService.saveCustomer(editedCustomer.value);
        customers.value.push(editedCustomer.value);
      }

      dialog.value = false;
      await getCustomers();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Customer ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }
  function useCustomer(customer: Customer) {
    usingCus.value = JSON.parse(JSON.stringify(customer));
    status.value = true;
    dialog2.value = false;
  }
  const cusID = usingCus.value.id;
  async function deleteCustomer(id: number) {
    const confirmResult = await Swal.fire({
      title: "Are you sure?",
      text: "You will not be able to recover this customer!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes",
    });

    if (confirmResult.isConfirmed) {
      loadingStore.isLoading = true;
      try {
        const res = await customerService.deleteCustomer(id);
        await getCustomers();
        messageStore.showConfirm("ลบ Customer สำเร็จ");
      } catch (e) {
        console.log(e);
        messageStore.showError("ไม่สามารถลบ Customer ได้");
      }
      loadingStore.isLoading = false;
    }
  }
  function editCustomer(customer: Customer) {
    editedCustomer.value = JSON.parse(JSON.stringify(customer));
    dialog.value = true;
  }

  return {
    customers,
    getCustomers,
    dialog,
    dialog2,
    editedCustomer,
    saveCustomer,
    editCustomer,
    useCustomer,
    deleteCustomer,
    status,
    usingCus,
    keyword,
    page,
    // take,
    getReportCustomer,
    getCustomerByPoint,
    customerBypoint,
    cusID,
    customerReport,
  };
});
