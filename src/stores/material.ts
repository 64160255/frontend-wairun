import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Material from "@/types/Material";
import materialService from "@/services/material";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import Swal from "sweetalert2";
export const useMaterialStore = defineStore("Material", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const materials = ref<Material[]>([]);
  const editedMaterial = ref<Material>({
    name: "",
    min_quantity: 0,
    quantity: 0,
    unit: "",
    price_per_unit: 0,
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedMaterial.value = {
        name: "",
        min_quantity: 0,
        quantity: 0,
        unit: "",
        price_per_unit: 0,
      };
    }
  });
  async function getMaterials() {
    loadingStore.isLoading = true;
    try {
      const res = await materialService.getMaterials();
      materials.value = res.data.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Material ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveMaterials() {
    loadingStore.isLoading = true;
    try {
      if (editedMaterial.value.id) {
        const res = await materialService.updateMaterials(
          editedMaterial.value.id,
          editedMaterial.value
        );
        console.log(editedMaterial);
      } else {
        const res = await materialService.saveMaterials(editedMaterial.value);
      }

      dialog.value = false;
      await getMaterials();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Material ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteMaterials(id: number) {
    const confirmed = await Swal.fire({
      title: "Are you sure?",
      text: "You will not be able to recover this material!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes",
    });

    if (confirmed.isConfirmed) {
      loadingStore.isLoading = true;
      try {
        const res = await materialService.deleteMaterials(id);
        await getMaterials();
        messageStore.showConfirm("Material deleted successfully");
      } catch (e) {
        console.log(e);
        messageStore.showError("Failed to delete material");
      }
      loadingStore.isLoading = false;
    }
  }
  function editMaterials(material: Material) {
    editedMaterial.value = JSON.parse(JSON.stringify(material));
    dialog.value = true;
  }
  return {
    materials,
    getMaterials,
    dialog,
    editedMaterial,
    saveMaterials,
    editMaterials,
    deleteMaterials,
  };
});
